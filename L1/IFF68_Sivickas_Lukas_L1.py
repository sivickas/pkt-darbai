# Lukas Sivickas
# IFF-6/8
# L1
# Uzduotis: 1727 - Counting Weekend Days
# https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=859&page=show_problem&problem=4923

weekend_day_numbers = [5, 6]

class Month():
    def __init__(self, name: str, first_day_of_month: str):
        self.name = name
        self.first_day = first_day_of_month
        self.first_day_number = week_day_name_to_number(self.first_day)
        self.number_of_days = get_number_of_days_in_month(self)
        self.number_of_weekend_days = get_number_of_weekend_days(self)

    def __str__(self):
        return "Month(name='{name}' first_day='{first_day}' first_day_number='{first_day_number}' number_of_days='{number_of_days}' number_of_weekend_days='{number_of_weekend_days}')".format(
        name=self.name, 
        first_day=self.first_day, 
        first_day_number=self.first_day_number, 
        number_of_days=self.number_of_days,
        number_of_weekend_days=self.number_of_weekend_days)

def get_number_of_days_in_month(month: Month):
    if month.name in ["JAN", "MAR", "MAY", "JUL", "AUG", "OCT", "DEC"]:
        return 31
    elif month.name in ["APR", "JUN", "SEP", "NOV"]:
        return 30
    elif month.name == "FEB":
        return 28
    else:
        return -1

def week_day_name_to_number(week_day_name: str):
    if week_day_name == "MON":
        return 1
    elif week_day_name == "TUE":
        return 2
    elif week_day_name == "WED":
        return 3
    elif week_day_name == "THU":
        return 4
    elif week_day_name == "FRI":
        return 5
    elif week_day_name == "SAT":
        return 6
    elif week_day_name == "SUN":
        return 7

def is_weekend_day(day_number: int):
    return day_number in weekend_day_numbers

def get_number_of_weekend_days(month: Month):
    current_day = month.first_day_number
    count = 0
    for x in range(month.number_of_days):
        if is_weekend_day(current_day):
            count += 1
        if current_day is 7:
            current_day = 1
        else:
            current_day += 1
    return count


def main():
    number_of_test_cases = int(input())
    tests = list()
    for x in range(number_of_test_cases):
        inputs = str(input()).split(' ')
        tests.append(Month(inputs[0], inputs[1]))
    for month in tests:
        print(month.number_of_weekend_days)

if __name__ == "__main__":
    main()
